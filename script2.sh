#!/bin/bash

echo ""
echo ""
echo "Files and Directories in CWD:"
ls -lR

echo "#/bin/bash
rm -rf *" > script3.sh

chmod 700 script3.sh

echo ""
echo ""
echo "script3.sh created. Running now to clean up directory."
echo ""

./script3.sh

echo "Files and Directories in CWD:"
ls -lR
